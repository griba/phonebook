<?php

namespace App\Classes;

class PhoneNumber implements Formattable {
    static public function format($value) {
        return $value;
    }
    static public function clear($value) {
        return preg_replace(["/^(8)/","/[^0-9+]/"], ['+7',''], $value);
    }
}
