<?php

namespace App\Classes;

interface Formattable {
    static function format($value);
    static function clear($value);
}
