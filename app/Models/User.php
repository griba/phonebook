<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $table = 'users';
    
    public function details() {
        return $this->hasOne('App\Models\UserDetails','user_id','id');
    }
    public function favourites() {
        return $this->hasMany('App\Models\UserFavourite','user_id','id');
    }
}
