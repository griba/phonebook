<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Classes\PhoneNumber;

class UserDetails extends Model
{
    protected $table = 'users_details';
    
    protected $primaryKey = 'user_id';
    
    public $timestamps = false;
    
    protected $touches = ['user'];
    
    protected $fillable = [
        'first_name', 'last_name', 'phone'
    ];
    
        
    public function user() {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function favourite() {
        return $this->hasOne('App\Models\UserFavourite','favourite_user_id','user_id')->where('user_id',auth()->user()->id);
    }
    
    public function setPhoneAttribute($value) {
          $this->attributes['phone'] = PhoneNumber::clear($value);
    }

}
