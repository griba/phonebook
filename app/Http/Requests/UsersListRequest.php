<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UsersListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'phone' => ['nullable','string','regex:/^[0-9\+()\-\s]+$/'],
            'page' => 'nullable|integer',
            'sort_column' => 'nullable|in:first_name,last_name,phone', // Вместо этого можно сделать проверку на существование поля
            'sort_order' => 'nullable|in:asc,desc',
        ];
    }
}
