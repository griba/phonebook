<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Models\UserFavourite;
use App\Http\Resources\UserFavouritesResource;
use Illuminate\Support\Facades\Gate;

class UserFavouriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Requests\UsersListRequest $request)
    {
        $users = UserFavourite::with('favouriteDetails')->where('user_id',auth()->user()->id);

        return UserFavouritesResource::collection($users->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\UserFavouriteStoreRequest $request)
    {
        try {
            if (Gate::allows('create', UserFavourite::class)) {
                $userFavourite = UserFavourite::create($request->only(['user_id', 'favourite_user_id']));
                return response()->json($userFavourite,201);
            } else {
                return response()->json([],405);
            }
        } catch (\Exception $e) {
            return response()->json([],500);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserFavourite  $UserFavourite
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserFavourite $userFavourite)
    {
        try {
            if (Gate::allows('delete', $userFavourite)) {
                $userFavourite->delete(); 
                return response()->json([],200);
            } else {
                return response()->json([],405);
            }
        } catch (\Exception $e) {
            return response()->json([],500);
        }
    }
}
