<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserDetails;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Services\AgentRelationsService;

class UserDetailsPolicy
{
    use HandlesAuthorization;

    public function create(User $user) {
        return true;
    }
    
    public function view(User $user, UserDetails $details)
    {
        return true;
    }
    
    public function update(User $user, UserDetails $details)
    {
        return $user->id == $details->user_id;
    }
    
    public function delete(User $user, UserDetails $details)
    {
        return $user->id == $details->user_id;
    }
}
