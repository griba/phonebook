<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = factory(App\Models\User::class, 100000)->make();
        
        foreach ($users as $user) {
            repeat1:
            try {
                $user->save();
            } catch (\Illuminate\Database\QueryException $e) {
                $user = factory(App\Models\User::class)->make();
                goto repeat1;
            }
            
            $user->details()->save(factory(App\Models\UserDetails::class)->make());
            
            $favourites = factory(App\Models\UserFavourite::class,30)->make();
            foreach ($favourites as $favourite) {
                repeat2:
                try {
                    $user->favourites()->save($favourite);
                } catch (\Illuminate\Database\QueryException $e) {
                    $favourite = factory(App\Models\UserFavourite::class)->make();
                    goto repeat2;
                }
            }
        }
    }
}